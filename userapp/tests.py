from rest_framework.test import APITestCase
from django.contrib.auth import get_user_model
from rest_framework import status
from rest_framework.test import APIClient

class SignupAPITest(APITestCase):
    def setUp(self):
        self.client = APIClient()

    def test_api_can_create_a_new_user(self):
        # here creating a new user
        new_client = APIClient()
        response = new_client.post('/user/signup/',{"first_name": "soumen","last_name":"khatua",
            "email":"soumen12346@gmail.com","password":"soumen1234","confirm_password":"soumen1234"},format="json")

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_api_to_check_by_not_providing_email(self):
        # here checking by not providing the email
        new_client = APIClient()
        response = new_client.post('/user/signup/',{"first_name": "soumen","last_name":"khatua",
            "password":"soumen1234","confirm_password":"soumen1234"},format="json")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_api_to_check_by_not_providing_same_password(self):
        # here checking both password are same or not
        new_client = APIClient()
        response = new_client.post('/user/signup/',{"first_name": "soumen","last_name":"khatua",
            "email":"soumen12346@gmail.com","password":"soumen12","confirm_password":"soumen1234"},format="json")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
