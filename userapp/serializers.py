from rest_framework import serializers
# from .models import Use
from django.contrib.auth import get_user_model


class UserSerializer(serializers.ModelSerializer):
    confirm_password = serializers.CharField(label='Confirm Password')
    def create(self, validated_data):
        user = get_user_model().objects.create(
            first_name =  validated_data['first_name'],
            last_name =  validated_data['last_name'],
            email = validated_data['email']
        )
        user.set_password(validated_data['password'])
        user.save()
        return user

    class Meta:
        model = get_user_model()
        fields = ('user_id','email','password','first_name','last_name','confirm_password')
        extra_kwargs = {
            'user_id':  {'read_only' : True},
            'password': {'write_only': True},
            'confirm_password': {'write_only': True},
        }

    def validate(self, attrs):
        password = attrs.get('password')
        confirm_password = attrs.pop('confirm_password')
        # password length checking
        if len(password) < 8:
            message = {
                "password": 'This password is too short. It must contain at least 8 characters.'
            }
            raise serializers.ValidationError(message)

        # password validation
        if password != confirm_password:
            message = {
                "password": 'Password not matched!!!'
            }
            raise serializers.ValidationError(message)
        return attrs
