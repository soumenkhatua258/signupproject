# django imports
from django.contrib.auth import login, authenticate
from .serializers import UserSerializer
# DRF imports
from rest_framework.views import APIView
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework import status
from rest_framework.authtoken.models import Token


class SignupAPIView(APIView):
    """ User needs to provide all model field to sign up """
    permission_classes = (AllowAny,)
    def post(self, request, *args, **kwargs):
        data = request.data
        serializer = UserSerializer(data = data)
        if serializer.is_valid(raise_exception = True):
            user = serializer.save()
            token = Token.objects.get(user = user)
            context = {
                "message" : "User created successfully.",
                'status' : "Success",
                "token" : token.key
            }
            return Response(context, status = status.HTTP_201_CREATED)
        return Response({"message" : "Bad Request",'status': "failure"}, status = status.HTTP_400_BAD_REQUEST)
