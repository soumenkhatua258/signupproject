from django.db import models
from django.contrib.auth.models import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.conf import settings
from django.db.models.signals import post_save
from django.dispatch import receiver

# app level imports
from .managers import UserManager


# drf level imports
from rest_framework.authtoken.models import Token

class User(AbstractBaseUser, PermissionsMixin):
    """
    Custom user model where email is the unique identifierfor authentication instead of usernames.
    This will later be linked to a Profile, if it is needed.
    """

    user_id = models.AutoField(primary_key=True,db_index = True)
    email = models.EmailField(unique=True, db_index=True)
    first_name = models.CharField(max_length = 150)
    last_name = models.CharField(max_length = 150)
    is_staff = models.BooleanField(default=False)
    is_active = models.BooleanField(default = True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    objects = UserManager()

# to create token for each new user by using django signal
@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user = instance)
