# This is a demo project and here I'm using default sqlite3 database
### create one  virtual enviroment and run this command
```
pip install -r requirements.txt
```
### Run these Following command to make the database schema
```
python manage.py makemigrations
python manage.py migrate
```

### To run the project type this  below command
```
python manage.py runserver
```

### To create a new user follow this url and pass these below paramter
1. Password must contain at least 8 characters. 
2. Both password should be same
3. Email should be unique

* http://127.0.0.1:8000/user/signup/
```
{
	"first_name": "Name",
	"last_name":"Title",
	"email":"name@example.com",
	"password":"name1234",
	"confirm_password":"name1234"
}

```

### To do the unit testing run this below command
```
python manage.py test
```




